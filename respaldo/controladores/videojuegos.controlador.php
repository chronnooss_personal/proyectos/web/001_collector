<?php

    require_once "../modelos/videojuegos.modelo.php";

    if(isset( $_POST['function'] )){

        switch ($_POST['function']) {

            case "opcionBuscarVideojuego":

                $fields = "id, name, cover.*";
                $search = $_POST['search'];
                $platform = $_POST['platform'];
                $genre = $_POST['genre'];
                $theme = $_POST['theme'];
                $limit = $_POST['limit'];

                $responseToken = ModeloVideojuegos::mdlObtenerToken();
                $token = $responseToken -> {'access_token'};

                $resultadoBase = (array) ModeloVideojuegos::MdlBuscarVideojuego($fields, $search, $platform, $genre, $theme, $limit, $token);
                echo json_encode($resultadoBase);
                return true;
                break;

            case "opcionDetalleVideojuego":

                $fields = " id,
                            name, 
                            first_release_date, 
                            involved_companies.*,
                            involved_companies.company.*,
                            themes.*,
                            genres.*,
                            platforms.*,
                            game_modes.*,
                            websites.*,
                            cover.image_id,
                            screenshots.*,
                            franchises.*,
                            franchises.games.*,
                            franchises.games.cover.*,
                            collection.*,
                            collection.games.*,
                            collection.games.cover.*,
                            similar_games.*,
                            similar_games.cover.*
                          ";

                $id = $_POST['id'];

                $responseToken = ModeloVideojuegos::mdlObtenerToken();
                $token = $responseToken -> {'access_token'};

                $resultadoBase = (array) ModeloVideojuegos::MdlDetalleVideojuego($fields, $id, $token);
                echo json_encode($resultadoBase);
                return true;
                break;
        }
    }

    
    
?>
