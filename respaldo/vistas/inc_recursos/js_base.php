<!--=============================================
=            Include JavaScript files           =
==============================================-->

<!-- Bootstrap V5 -- Option 1: Bootstrap Bundle with Popper -->
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js"
    integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM"
    crossorigin="anonymous"></script>
<!-- Bootstrap V5 -- Option 2: Separate Popper and Bootstrap JS -->
<!-- <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.2/dist/umd/popper.min.js" integrity="sha384-IQsoLXl5PILFhosVNubq5LC7Qb9DXgDA9i+tQ8Zj3iwWAwPtgFTxbJ8NT4GN1R8p" crossorigin="anonymous"></script> -->
<!-- <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.min.js" integrity="sha384-cVKIPhGWiC2Al4u+LWgxfKTRIcfu0JTxR+EQDz/bgldoEyl4H0zUF0QKbrJ0EcQF" crossorigin="anonymous"></script> -->

<!-- Plugins: Jquery -->
<script type="text/javascript" src="<?php echo SERVERURL; ?>vistas/assets/js/plugins/jquery-3.5.1.js"></script>

<!-- Plugins: Datatable -->
<script type="text/javascript" src="<?php echo SERVERURL; ?>vistas/assets/js/plugins/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="<?php echo SERVERURL; ?>vistas/assets/js/plugins/dataTables.bootstrap5.min.js"></script>

<!-- Plugins: Datatable Translation -->
<script type="text/javascript" src="<?php echo SERVERURL; ?>vistas/assets/js/plugins/datatable.spanish.js"></script>

<!-- Plugins: Rater Js -->
<script type="text/javascript" src="<?php echo SERVERURL; ?>vistas/assets/js/plugins/rater-js/rater-js.js"></script>
<script type="text/javascript" src="<?php echo SERVERURL; ?>vistas/assets/js/plugins/rater-js.js"></script>

<!-- Plugins: Toastr Js -->
<script type="text/javascript" src="<?php echo SERVERURL; ?>vistas/assets/js/plugins/toastr.min.js"></script>
<!-- Plugins: Owl Carousel -->
<script type="text/javascript" src="<?php echo SERVERURL; ?>vistas/assets/js/plugins/owl.carousel.min.js"></script>

<!-- Custom Js -->
<script type="text/javascript" src="<?php echo SERVERURL; ?>vistas/assets/js/main.js"></script>

<!--=============================================
=            Include Ajax files           =
==============================================-->
<script type="text/javascript" src="<?php echo SERVERURL; ?>vistas/assets/js/ajax/game_db.ajax.js"></script>