async function obtCatalogoVideojuegos() {
   $("#formBusquedaJuegos").submit(async (e) => {
      e.preventDefault();

      const url = "./controladores/videojuegos.controlador.php";

      const postData = {
         function: "opcionBuscarVideojuego",
         search: $("#txtBusquedaJuego").val(),
         platform: $("#selPlataformas option:selected").val(),
         genre: $("#selGeneros option:selected").val(),
         theme: $("#selTemas option:selected").val(),
         limit: $("#selLimite").val(),
      };

      $.post(url, postData, (response) => {
         document.getElementById("checkBiblioteca__galeria").innerHTML = "";

         var flexIm = "";
         var f = "";

         $.each(JSON.parse(response), function (i, item) {
            flexIm = "";
            flexIm += ``;

            flexIm += `<div class="galeria_item">`;

            if (item.gameUrlCoverImage != "") {
               flexIm += `       <img class='galeria_imagen' src="${item.gameUrlCoverImage}" alt="${item.gameName}" title="${item.gameName}">
                                 <div class='galeria_overlay'>
                                    <p class="galeria__title">${item.gameName}</p>
                                    <div class="galeria__description">
                                       <a href="#" data-bs-toggle="modal" data-bs-whatever="${item.gameId}" data-bs-target="#modalBusquedaVideojuegos"><i class="fas fa-info-circle btnInfo"></i></a>
                                       <a href="#" onClick="notificacionSuccess(event)"><i class="fas fa-plus-circle btnAgregar"></i></a>
                                    </div>
                                 </div>`;
            } else {
               flexIm += `       <img class='galeria_imagen' src='https://st3.depositphotos.com/23594922/31822/v/600/depositphotos_318221368-stock-illustration-missing-picture-page-for-website.jpg' alt="${item.gameName}" title="${item.gameName}">
                                 <div class='galeria_overlay'>
                                    <p class="galeria__title">${item.gameName}</p>
                                    <div class="galeria__description">
                                       <a href="#" data-bs-toggle="modal" data-bs-whatever="${item.gameId}" data-bs-target="#modalBusquedaVideojuegos"><i class="fas fa-info-circle btnInfo"></i></a>
                                       <a href="#" onClick="notificacionSuccess(event)"><i class="fas fa-plus-circle btnAgregar"></i></a>
                                    </div>
                                 </div>`;
            }
            flexIm += `</div>`;

            f += flexIm;
         });

         var cBiblioteca = document.getElementById("checkBiblioteca");
         cBiblioteca.style.display = "block";

         document.getElementById("checkBiblioteca__galeria").innerHTML += f;
      });
   });
   return false;
}

// Llamada del modal
var modalDetalleVideojuegos = document.getElementById("modalBusquedaVideojuegos");
modalDetalleVideojuegos.addEventListener("show.bs.modal", function (event) {
   document.getElementById("modal__info--name").innerHTML = "";
   document.getElementById("modal__info--firstDateRelease").innerHTML = "";
   document.getElementById("modal__info--developer").innerHTML = "";
   document.getElementById("modal__info--publisher").innerHTML = "";
   document.getElementById("modal__info--themes").innerHTML = "";
   document.getElementById("modal__info--genres").innerHTML = "";
   document.getElementById("modal__info--platforms").innerHTML = "";
   document.getElementById("modal__info--gameModes").innerHTML = "";
   document.getElementById("modal__info--webSites").innerHTML = "";
   document.getElementById("modal__image--cover").innerHTML = "";
   document.getElementById("modal__screenshots--carousel").innerHTML = "";
   document.getElementById("modal_franchises--carousel").innerHTML = "";
   document.getElementById("modal_series--carousel").innerHTML = "";
   document.getElementById("modal_similar--carousel").innerHTML = "";

   var button = event.relatedTarget;
   var idVideojuego = button.getAttribute("data-bs-whatever");

   const url = "./controladores/videojuegos.controlador.php";

   const postData = {
      function: "opcionDetalleVideojuego",
      id: idVideojuego,
   };

   $.post(url, postData, (response) => {
      // modal__info
      res_name = "";
      res_firstDateRelease = "";
      res_developer = "";
      res_publisher = "";
      res_themes = "";
      res_genres = "";
      res_platforms = "";
      res_gameModes = "";
      res_webSites = "";
      // modal__image
      res_coverImageUrl = "";
      // modal__screenshots--carousel
      res_carouselImages = "";
      // modal_franchises--carousel
      res_carouselFranchises = "";
      // modal_Series--carousel
      res_carouselSeries = "";
      // modal_similar--carousel
      res_carouselSimilar = "";

      $.each(JSON.parse(response), function (i, item) {
         // modal__info
         res_name = `<h6>${item.game_name}</h6>`;
         res_firstDateRelease = `<h6>${item.game_firstDateRelease}</h6>`;
         for (var x = 0; x < item.game_involvedCompanies.length; x++) {
            if (item.game_involvedCompanies[x]["isDeveloper"]) {
               res_developer += ` <span class='badge bg-platform bg-platform_stadia mx-1'>${item.game_involvedCompanies[x]["nameCompany"]}</span>`;
            }
         }
         for (var x = 0; x < item.game_involvedCompanies.length; x++) {
            if (item.game_involvedCompanies[x]["isPublisher"]) {
               res_publisher += ` <span class='badge bg-platform bg-platform_gb mx-1'>${item.game_involvedCompanies[x]["nameCompany"]}</span>`;
            }
         }
         for (var x = 0; x < item.game_themes.length; x++) {
            res_themes += ` <span class='badge bg-danger mx-1'>${item.game_themes[x]["nameTheme"]}</span>`;
         }
         for (var x = 0; x < item.game_genres.length; x++) {
            res_genres += ` <span class='badge bg-success mx-1'>${item.game_genres[x]["nameGenre"]}</span>`;
         }
         for (var x = 0; x < item.game_platforms.length; x++) {
            res_platforms += ` <span class='badge bg-platform ${item.game_platforms[x]["classPlatform"]} mx-1'>${item.game_platforms[x]["abbrevNamePlatform"]}</span>`;
         }
         for (var x = 0; x < item.game_gameModes.length; x++) {
            res_gameModes += ` <span class='badge bg-info mx-1'>${item.game_gameModes[x]["nameGameMode"]}</span>`;
         }
         for (var x = 0; x < item.game_webSites.length; x++) {
            res_webSites += ` <a href="${item.game_webSites[x]["urlWebsite"]}" class='badge bg-dark mx-1 link'>${item.game_webSites[x]["nameWebsite"]}</a>`;
         }

         // modal__image
         res_coverImageUrl = `<div>
                                 <img src="${item.game_coverImageUrl}" alt="${item.game_name}" title="${item.game_name}">
                              </div>`;

         // modal__screenshots--carousel
         for (var x = 0; x < item.game_screenshotImageUrl.length; x++) {
            res_carouselImages += `<div class="single-item">
                                       <img src="${item.game_screenshotImageUrl[x]["urlScreenshot"]}" alt="">
                                    </div>`;
         }
         // modal_franchises--carousel
         for (var x = 0; x < item.game_franchises.length; x++) {
            res_carouselFranchises += `<div class="single-item">
                                       <img src="${item.game_franchises[x]["urlImage"]}" alt="${item.game_franchises[x]["nameGame"]}" title="${item.game_franchises[x]["nameGame"]}">
                                    </div>`;
         }
         // modal_series--carousel
         for (var x = 0; x < item.game_collections.length; x++) {
            res_carouselSeries += `<div class="single-item">
                                       <img src="${item.game_collections[x]["urlImage"]}" alt="${item.game_collections[x]["nameGame"]}" title="${item.game_collections[x]["nameGame"]}">
                                    </div>`;
         }
         // modal_similar--carousel
         for (var x = 0; x < item.game_similarGames.length; x++) {
            res_carouselSimilar += `<div class="single-item">
                                       <img src="${item.game_similarGames[x]["urlImage"]}" alt="${item.game_similarGames[x]["nameGame"]}" title="${item.game_similarGames[x]["nameGame"]}">
                                    </div>`;
         }
      });

      // modal__info
      document.getElementById("modal__info--name").innerHTML = res_name;
      document.getElementById("modal__info--firstDateRelease").innerHTML = res_firstDateRelease;
      document.getElementById("modal__info--developer").innerHTML = res_developer;
      document.getElementById("modal__info--publisher").innerHTML = res_publisher;
      document.getElementById("modal__info--themes").innerHTML = res_themes;
      document.getElementById("modal__info--genres").innerHTML = res_genres;
      document.getElementById("modal__info--platforms").innerHTML = res_platforms;
      document.getElementById("modal__info--gameModes").innerHTML = res_gameModes;
      document.getElementById("modal__info--webSites").innerHTML = res_webSites;
      // modal__image
      document.getElementById("modal__image--cover").innerHTML = res_coverImageUrl;
      // modal__screenshots--carousel
      document.getElementById("modal__screenshots--carousel").innerHTML = res_carouselImages;
      // modal_franchises--carousel
      document.getElementById("modal_franchises--carousel").innerHTML = res_carouselFranchises;
      // modal_series--carousel
      document.getElementById("modal_series--carousel").innerHTML = res_carouselSeries;
      // modal_similar--carousel
      document.getElementById("modal_similar--carousel").innerHTML = res_carouselSimilar;

      // $(".feed-area-images").owlCarousel("destroy");
      $(".feed-area-images").trigger("destroy.owl.carousel");
      // $(".feed-area-images").empty();
      $(".feed-area-images").owlCarousel({
         items: 3,
         // loop: true,
         autoplay: true,
         margin: 10,
         smartSpeed: 1000,
         autoplayTimeout: 3000,
         autoHeight: true,
         // merge: true,
         // autoWidth: true,
         responsive: {
            0: {
               items: 2,
            },
            576: {
               items: 3,
            },
            768: {
               items: 3,
            },
            992: {
               items: 3,
            },
            1200: {
               items: 3,
            },
         },
      });

      $(".feed-area").trigger("destroy.owl.carousel");
      $(".feed-area").owlCarousel({
         items: 6,
         // loop: true,
         autoplay: true,
         margin: 10,
         smartSpeed: 1000,
         autoplayTimeout: 3000,
         autoHeight: true,
         // merge: true,
         // autoWidth: true,
         responsive: {
            0: {
               items: 2,
            },
            576: {
               items: 3,
            },
            768: {
               items: 4,
            },
            992: {
               items: 4,
            },
            1200: {
               items: 4,
            },
         },
      });
   });
});

$(document).ready(function () {
   obtCatalogoVideojuegos();
});
