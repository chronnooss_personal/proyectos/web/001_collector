$(document).ready(function () {
   toastr.options = {
      closeButton: true,
      debug: false,
      newestOnTop: true,
      progressBar: true,
      positionClass: "toast-top-right",
      preventDuplicates: false,
      onclick: null,
      showDuration: "500",
      hideDuration: "1000",
      timeOut: "5000",
      extendedTimeOut: "1000",
      showEasing: "swing",
      hideEasing: "linear",
      showMethod: "fadeIn",
      hideMethod: "fadeOut",
   };
});
// Sidebar
let arrow = document.querySelectorAll(".arrow");
for (var i = 0; i < arrow.length; i++) {
   arrow[i].addEventListener("click", (e) => {
      let arrowParent = e.target.parentElement.parentElement; //selecting main parent of arrow
      arrowParent.classList.toggle("showMenu");
   });
}
let sidebar = document.querySelector(".sidebar");
let sidebarBtn = document.querySelector(".bx-menu");
console.log(sidebarBtn);
sidebarBtn.addEventListener("click", () => {
   sidebar.classList.toggle("close");
});

// Tooltip
var tooltipTriggerList = [].slice.call(document.querySelectorAll('[data-bs-toggle="tooltip"]'));
var tooltipList = tooltipTriggerList.map(function (tooltipTriggerEl) {
   return new bootstrap.Tooltip(tooltipTriggerEl);
});

// Dark mode
const chk = document.getElementById("chk");
chk.addEventListener("change", () => {
   document.body.classList.toggle("dark");
});

// Toastr JS
function notificacionSuccess(e) {
   e.preventDefault();
   toastr["success"]("Se ha agregado el título a tu lista.", "Notificación");
   // toastr["info"]("Puedes agregar este título a tu lista.", "Notificación");
   // toastr["warning"]("Este título ya se ha agregado a tu lista.", "Notificación");
   // toastr["danger"]("Ha ocurrido un error al intentar agregar el título a tu lista. Intentalo de nuevo.", "Notificación");
}

function visibilidadJuegosBusqueda() {
   var checkBox1 = document.getElementById("checkRadioBusquedaJuego1");
   var checkBox2 = document.getElementById("checkRadioBusquedaJuego2");

   var cTabla = document.getElementById("checkTabla");
   var cBiblioteca = document.getElementById("checkBiblioteca");

   if (checkBox1.checked == true) {
      cTabla.style.display = "block";
   } else {
      cTabla.style.display = "none";
   }

   if (checkBox2.checked == true) {
      cBiblioteca.style.display = "block";
   } else {
      cBiblioteca.style.display = "none";
   }
}

var tooltipTriggerList = [].slice.call(document.querySelectorAll('[data-bs-toggle="tooltip"]'));
var tooltipList = tooltipTriggerList.map(function (tooltipTriggerEl) {
   return new bootstrap.Tooltip(tooltipTriggerEl);
});
