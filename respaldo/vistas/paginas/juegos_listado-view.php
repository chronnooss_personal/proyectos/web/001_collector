<div class="main__content listadoVideojuegos">
    <div class="info_1 card">
        <i class="fas fa-shopping-bag"></i>
        <p>Adquiridos</p>
        <p>25</p>
    </div>

    <div class="info_2 card">
        <span><i class="fas fa-download"></i></span>
        <p>Descargados</p>
        <p>100</p>
    </div>

    <div class="info_3 card">
        <span><i class="fas fa-check-circle"></i></span>
        <p>Terminados</p>
        <p>3</p>
    </div>

    <div class="info_4 card">
        <span><i class="fab fa-youtube"></i></span>
        <p>Youtube</p>
        <p>9</p>
    </div>

    <div class="seccion_formulario card">
        <form id="formListadoVideojuegos" class="formBusqueda">
            <h3>Mi biblioteca de videojuegos</h3>

            <div class="entradas">
                <div>
                    <label for="busquedaJuego" class="form-label">Título</label>
                    <input type="text" class="form-control" id="txtBusquedaJuego" aria-label="Título">

                </div>

                <div>
                    <label for="limiteBusquedaJuego" class="form-label">Límite</label>
                    <input type="number" id="selLimite" class="form-control" value="20" min="1" max="500" />
                </div>

                <div>
                    <label for="selPlataformas" class="form-label">Plataforma</label>
                    <select class="form-select" id="selPlataformas">
                        <option value="0">Todas las plataformas</option>
                        <optgroup label="Nintendo">
                            <option disabled>Consolas de mesa</option>
                            <option value="18">Nintendo</option>
                            <option value="19">Super Nintendo</option>
                            <option value="4">Nintendo 64</option>
                            <option value="21">Nintendo GameCube</option>
                            <option value="5">Nintendo Wii</option>
                            <option value="41">Nintendo Wii U</option>
                            <option value="130">Nintendo Switch</option>
                            <option disabled>Consolas portatiles</option>
                            <option value="33">Game Boy</option>
                            <option value="22">Game Boy Color</option>
                            <option value="24">Game Boy Advance</option>
                            <option value="20">Nintendo DS</option>
                            <option value="159">Nintendo DSi</option>
                            <option value="37">Nintendo 3DS</option>
                            <option value="137">Nintendo New 3ds</option>
                        </optgroup>
                        <optgroup label="Sony">
                            <option disabled>Consolas de mesa</option>
                            <option value="7">PlayStation 1</option>
                            <option value="8">PlayStation 2</option>
                            <option value="9">PlayStation 3</option>
                            <option value="48">PlayStation 4</option>
                            <option value="167">PlayStation 5</option>
                            <option disabled>Consolas portatiles</option>
                            <option value="38">PlayStation Portatil</option>
                            <option value="46">PlayStation Vita</option>
                        </optgroup>
                        <optgroup label="Microsoft">
                            <option disabled>Consolas de mesa</option>
                            <option value="11">Xbox</option>
                            <option value="12">Xbox 360</option>
                            <option value="49">Xbox One</option>
                            <option value="169">Xbox Series</option>
                        </optgroup>
                    </select>
                </div>

                <div>
                    <label for="selEstado" class="form-label">Estado</label>
                    <select class="form-select" id="selEstado">
                        <option value="0">Todos</option>
                        <option value="1">No adquirido</option>
                        <option value="2">Edición física</option>
                        <option value="3">Edición digital</option>
                        <option value="4">Edición coleccionista</option>
                        <option value="5">Rom</option>
                    </select>
                </div>

                <div>
                    <label for="selProgreso" class="form-label">Progreso</label>
                    <select class="form-select" id="selProgreso">
                        <option value="0">Todos</option>
                        <option value="1">No jugado</option>
                        <option value="2">Jugando actualmente</option>
                        <option value="3">Terminado</option>
                        <option value="4">Completado al 100%</option>
                        <option value="5">Por jugar después</option>
                    </select>
                </div>

                <div>
                    <label for="checkBusquedaJuego" class="form-label">Visualización</label>
                    <div id="checkBusquedaJuego">
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="radio" name="inlineRadioOptions"
                                id="checkRadioBusquedaJuego1" value="1" checked>
                            <label class="form-check-label" for="checkRadioBusquedaJuego1">Tabla</label>
                        </div>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="radio" name="inlineRadioOptions"
                                id="checkRadioBusquedaJuego2" value="2">
                            <label class="form-check-label" for="checkRadioBusquedaJuego2">Biblioteca</label>
                        </div>
                    </div>
                </div>
            </div>


            <div class="text-center">
                <button type="submit" id="buttonBusquedaJuego"
                    class="btn btn-info boton_enviar form-control">Buscar</button>
            </div>

        </form>
    </div>

    <div class="seccion_tops card">
        <p>Top 10</p>
    </div>

    <div class="seccion_tabla card">
        <div class="table-responsive">
            <table id="listaBusquedaJuegos" class="table table-bordered">
                <thead>
                    <th class="text-center px-1">Favoritos</th>
                    <th class="text-center px-1">Imagen</th>
                    <th class="w-auto text-center">Nombre</th>
                    <th class="w-auto text-center">Plataforma</th>
                    <th class="w-auto text-center">Genero</th>
                    <th class="w-auto text-center">Empresa</th>
                    <th class="w-auto text-center">Rating</th>
                    <th class="w-auto text-center">Estado</th>
                    <th class="w-auto text-center">Progreso</th>
                    <th class=" text-center">Youtube</th>
                    <th class="w-auto text-center">Acciones</th>
                </thead>
                <tbody>
                    <tr>
                        <td scope="row"></td>
                        <td>Not set</td>
                        <td>Pokémon Blue</td>
                        <td>Game Boy Color</td>
                        <td>RPG</td>
                        <td>Nintendo</td>
                        <td class="w-auto text-center px-1">
                            <div id="raterexample1"></div>
                        </td>
                        <td scope="row"></td>
                        <td class="w-auto text-center px-1"><i class="fas fa-times-circle"
                                style="color: #ac0000;"></i></td>
                        <td class="w-auto text-center px-1"><i class="fab fa-youtube" style="color: red;"></i>
                        </td>
                        <td class="w-auto text-center px-1">
                            <div class="d-inline-block">
                                <i class="fas fa-edit" style="color: #aaaaaa;"></i>
                                <i class="fas fa-trash" style="color: #aaaaaa;"></i>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td class="w-auto text-center px-1"><i class="fas fa-heart" style="color: #f47bff;"></i>
                        </td>
                        <td>Not set</td>
                        <td>Pokémon Blue</td>
                        <td>Game Boy Color</td>
                        <td>RPG</td>
                        <td>Nintendo</td>
                        <td class="w-auto text-center px-1">
                            <div id="raterexample2"></div>
                        </td>
                        <td class="w-auto text-center px-1"><i class="fas fa-arrow-alt-circle-down"
                                style="color: #009ac9;"></i>
                        </td>
                        <td class="w-auto text-center px-1"><i class="fas fa-check-circle"
                                style="color: #00ac00;"></i></td>
                        <td scope="row"></td>
                        <td class="w-auto text-center px-1">
                            <div class="d-inline-block">
                                <i class="fas fa-edit" style="color: #aaaaaa;"></i>
                                <i class="fas fa-trash" style="color: #aaaaaa;"></i>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td scope="row"></td>
                        <td>Not set</td>
                        <td>Pokémon Blue</td>
                        <td>Game Boy Color</td>
                        <td>RPG</td>
                        <td>Nintendo</td>
                        <td class="w-auto text-center px-1">
                            <div id="raterexample3"></div>
                        </td>
                        <td class="w-auto text-center px-1">
                            <i class="fas fa-shopping-bag" style="color: #ffae00;"></i>
                        </td>
                        <td class="w-auto text-center px-1">
                            <div class="progress">
                                <div class="progress-bar bg-warning" role="progressbar" style="width: 25%;"
                                    aria-valuenow="25" aria-valuemin="0" aria-valuemax="100">25%</div>
                            </div>
                        </td>
                        <td class="w-auto text-center px-1"><i class="fab fa-youtube" style="color: red;"></i>
                        </td>
                        <td class="w-auto text-center px-1">
                            <div class="d-inline-block">
                                <i class="fas fa-edit" style="color: #aaaaaa;"></i>
                                <i class="fas fa-trash" style="color: #aaaaaa;"></i>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td class="w-auto text-center px-1"><i class="fas fa-heart" style="color: #f47bff;"></i>
                        </td>
                        <td>Not set</td>
                        <td>Pokémon Blue</td>
                        <td>Game Boy Color</td>
                        <td>RPG</td>
                        <td>Nintendo</td>
                        <td class="w-auto text-center px-1">
                            <div id="raterexample4"></div>
                        </td>
                        <td class="w-auto text-center px-1"><i class="fas fa-arrow-alt-circle-down"
                                style="color: #009ac9;"></i>
                        </td>
                        <td class="w-auto text-center px-1">
                            <i class="fas fa-trophy" style="color: #a0ecfa;"></i>
                        </td>
                        <td scope="row"></td>
                        <td class="w-auto text-center px-1">
                            <div class="d-inline-block">
                                <i class="fas fa-edit" style="color: #aaaaaa;"></i>
                                <i class="fas fa-trash" style="color: #aaaaaa;"></i>
                            </div>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>

    <div class="seccion_recientes card">
        <p>Recién llegados</p>
    </div>
</div>