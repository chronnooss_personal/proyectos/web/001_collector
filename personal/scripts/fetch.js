(() => {
   ("use strict");

   // Obteniendo el token
   const obtenerToken = async() => {
      const clientid = "efbd78h8gkhku20i2assug435n3mpl";
      const secretid = "0il9c4mz6r4i5lkd6hcpxq4cjmma9q";

      const url = "https://id.twitch.tv/oauth2/token?client_id=" + clientid + "&client_secret=" + secretid + "&grant_type=client_credentials";
      const requestOptionsApi = {
         method: "POST",
         mode: "cors",
      };

      const respuesta = await fetch(url, requestOptionsApi);
      
      const datos = await respuesta.json();
      // console.log("obtenerToken", datos.access_token);
      return datos;
   }

   const getJuegos = async () => {
      const jsonToken = await obtenerToken();

      var respuesta = await fetch("https://cors-anywhere.herokuapp.com/https://api.igdb.com/v4/games", {
         method: "POST",
         // mode: "cors",
         headers: {
            Accept: "application/json",
            "Client-ID": "efbd78h8gkhku20i2assug435n3mpl",
            Authorization: `Bearer ${jsonToken.access_token}`,
         },
         body: `fields name, cover.image_id;
               search "Golden Sun";
               limit 500;`,
      });

      console.log({ respuesta });

      // Si la respuesta es correcta
      if (respuesta.status === 200) {
         const datos = await respuesta.json();

         datos.forEach((juego) => {
            const [
                     nombre,
                     image_url
                  ] =
                  [
                     juego.name,
                     `https://images.igdb.com/igdb/image/upload/t_720p/${juego.cover.image_id}.png`
                  ];
            
            console.log({ nombre, image_url });
         });
      } else {
         console.log("Ha ocurrido un error");
      }
   };

   getJuegos();

})();
