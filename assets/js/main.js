const checkGaleriaItem = (id) => {
   let item = document.getElementById(id);

   // Limpiar clases
   item.classList.remove("wantToPlay");
   item.classList.remove("playing");
   item.classList.remove("beaten");
   item.classList.remove("completed");
   item.classList.remove("archived");
   item.classList.remove("abandoned");

   let check_wantToPlay = document.querySelector("#wantToPlay");
   let check_playing = document.querySelector("#playing");
   let check_beaten = document.querySelector("#beaten");
   let check_completed = document.querySelector("#completed");
   let check_archived = document.querySelector("#archived");
   let check_abandoned = document.querySelector("#abandoned");

   if (check_wantToPlay.checked) {
      let checkId = check_wantToPlay.id;
      item.classList.add(checkId);
   }
   if (check_playing.checked) {
      let checkId = check_playing.id;
      item.classList.add(checkId);
   }
   if (check_beaten.checked) {
      let checkId = check_beaten.id;
      item.classList.add(checkId);
   }
   if (check_completed.checked) {
      let checkId = check_completed.id;
      item.classList.add(checkId);
   }
   if (check_archived.checked) {
      let checkId = check_archived.id;
      item.classList.add(checkId);
   }
   if (check_abandoned.checked) {
      let checkId = check_abandoned.id;
      item.classList.add(checkId);
   }
};
