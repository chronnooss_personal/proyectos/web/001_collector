/*--=============================================
=        Inicialización de variables           =
===============================================*/
const confDarkMode = () => {
   let chk = document.getElementById("chk");
   // chk.addEventListener("change", () => {
   //    document.body.classList.toggle("dark");
   // });
};
// Dark Mode
let chk = document.getElementById("chk");

// Sidebar
let arrow = document.querySelectorAll(".arrow");
let sidebar = document.querySelector(".sidebar");
let sidebarBtn = document.querySelector(".bx-menu");

// Tooltip
let tooltipTriggerList = [].slice.call(document.querySelectorAll('[data-bs-toggle="tooltip"]'));

/*--=============================================
=            Configuración           =
===============================================*/
// Toastr
const confToastr = (e) => {
   e.preventDefault();

   toastr.options = {
      closeButton: true,
      debug: false,
      newestOnTop: true,
      progressBar: true,
      positionClass: "toast-top-right",
      preventDuplicates: false,
      onclick: null,
      showDuration: "500",
      hideDuration: "1000",
      timeOut: "5000",
      extendedTimeOut: "1000",
      showEasing: "swing",
      hideEasing: "linear",
      showMethod: "fadeIn",
      hideMethod: "fadeOut",
   };
};

const confToastrSuccess = (e) => {
   toastr["success"]("Se ha agregado el título a tu lista.", "Notificación");
   // toastr["info"]("Puedes agregar este título a tu lista.", "Notificación");
   // toastr["warning"]("Este título ya se ha agregado a tu lista.", "Notificación");
   // toastr["danger"]("Ha ocurrido un error al intentar agregar el título a tu lista. Intentalo de nuevo.", "Notificación");
};

// Sidebar
const confSidebar = () => {
   for (var i = 0; i < arrow.length; i++) {
      arrow[i].addEventListener("click", (e) => {
         let arrowParent = e.target.parentElement.parentElement; //selecting main parent of arrow
         arrowParent.classList.toggle("showMenu");
      });
   }
};

// Tooltip
let tooltipList = tooltipTriggerList.map((tooltipTriggerEl) => {
   return new bootstrap.Tooltip(tooltipTriggerEl);
});

/*--=============================================
=            Eventos           =
===============================================*/
// Dark Mode
chk.addEventListener("change", () => {
   document.body.classList.toggle("dark");
});

// Sidebar
sidebarBtn.addEventListener("click", () => {
   sidebar.classList.toggle("close");
});

/*--=============================================
=            Llamado de funciones           =
===============================================*/
confToastr();
confDarkMode();
